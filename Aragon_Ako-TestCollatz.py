#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read - 3 tests
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "-5 243\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -5)
        self.assertEqual(j, 243)

    def test_read_3(self):
        s = "0 100000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  0)
        self.assertEqual(j, 100000)

    # ----
    # eval - 7 tests
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        with self.assertRaises(AssertionError):
            collatz_eval(-2, 2)
    
    def test_eval_6(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        with self.assertRaises(AssertionError):
            collatz_eval(25, -2)

    # -----
    # print - 1 test
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve - 1 test
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out

............
----------------------------------------------------------------------
Ran 12 tests in 0.005s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          30      0     12      0   100%
TestCollatz.py      51      0      6      0   100%
------------------------------------------------------------
TOTAL               81      0     18      0   100%

"""
#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read - 3 tests
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "-5 243\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  -5)
        self.assertEqual(j, 243)

    def test_read_3(self):
        s = "0 100000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  0)
        self.assertEqual(j, 100000)

    # ----
    # eval - 7 tests
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        with self.assertRaises(AssertionError):
            collatz_eval(-2, 2)
    
    def test_eval_6(self):
        with self.assertRaises(AssertionError):
            collatz_eval(5, 2)

    def test_eval_7(self):
        with self.assertRaises(AssertionError):
            collatz_eval(25, -2)

    # -----
    # print - 1 test
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve - 1 test
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out

............
----------------------------------------------------------------------
Ran 12 tests in 0.005s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          30      0     12      0   100%
TestCollatz.py      51      0      6      0   100%
------------------------------------------------------------
TOTAL               81      0     18      0   100%

"""
